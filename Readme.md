Emaglib
========

Data abstractions
-----------------

* we have the base type `Ticket` which comes by default as an abstract class
and exposes 2 field:
  - `from` (+ getter and setter)
  - `to`   (+getter and setter)
* There are 2 other classes `PlaneTicket` and `TrainTicket` which extend the base class `Ticket`. The logic behind this 
  is that each kind of ticket may have different attributes depending on the transportation type.
  
* There is also the `TicketBundle` class which internally stores a linked lists of tickets and provides the
the posibility to sort these tickets.
  - The `sortToDest()` method in `TicketBundle` will internally sort the tickets in the internal `LinkedList` and return
    a reference to the current object, allowing this way the possibility to chain methods.
    

Algorithm analysis
------------------

* At a very abstract level we can define a ticket as a pair of `(from, to)`. In order to connect 2 tickets 
  `(x1, x2)` and `(y1, y2)` one of 2 conditions has to be met:
  - `x2 == y1 => (x1, x2), (y1, y2)`
  - `y2 == x1 => (y1, y2), (x1, x2)`
* Because of this invariant we had the intuition that a modified **insertion sort** would be the most appropiate 
  solution for this problem:
  - Basically `TicketBundle.sortToDest` implements an **insertion sort** with the above invariant and returns a 
    reference to the current object in order to allow method chaining.
  - This will give a worst case `O(n^2)` time complexity and `O(n)` space complexity
  - If we optimize the second nesteed for to do not iterate from the beginning we can get average/best case complexity 
    of `O(n)`
    
Libraries and dependencies
--------------------------
* Junit 5 for unit tests
* Maven 4 for dependency management
* Java 8

How to run
-----------
* simply type `mvn clean test`
