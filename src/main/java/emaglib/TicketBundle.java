package emaglib;

import java.util.LinkedList;

public class TicketBundle {
    private LinkedList<Ticket> tickets = new LinkedList<>();

    public TicketBundle(LinkedList<Ticket> tickets) {
        this.tickets = tickets;
    }

    public TicketBundle() {
    }

    public TicketBundle sortToDest() {


        if (tickets.size() == 0) {
            return this;
        }

        LinkedList<Ticket> sorted = new LinkedList<>();
        sorted.addFirst(tickets.getFirst());

        for (Ticket t : tickets) {
            int i = 0;
            int insertIndex = -1;
            for (Ticket t2 : sorted) {
                if (t.getTo().equals(t2.getFrom())) {
                    insertIndex = i;
                } else if (t2.getTo().equals(t.getFrom())) {
                    insertIndex = i + 1;
                }
                ++i;
            }

            if (insertIndex != -1) {
                sorted.add(insertIndex, t);
            }
        }

        if (tickets.size() != sorted.size()) {
            sorted.add(tickets.getFirst());
        }
        this.tickets = sorted;
        return this;
    }

    public LinkedList<Ticket> getTickets() {
        return tickets;
    }

    public TicketBundle add(Ticket t) {
        tickets.add(t);
        return this;
    }

    public TicketBundle remove(Ticket t) {
        tickets.remove(t);
        return this;
    }

}
