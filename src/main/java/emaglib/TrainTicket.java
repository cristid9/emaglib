package emaglib;

public class TrainTicket extends Ticket {
    private String seatNumber;
    private String station;
    private String wagon;

    public TrainTicket(String seatNumber, String station, String wagon) {
        super();
        this.seatNumber = seatNumber;
        this.station = station;
        this.wagon = wagon;
    }

    public TrainTicket(String from, String to) {
        super(from, to);
    }

    public String getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(String seatNumber) {
        this.seatNumber = seatNumber;
    }

    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }

    public String getWagon() {
        return wagon;
    }

    public void setWagon(String wagon) {
        this.wagon = wagon;
    }
}
