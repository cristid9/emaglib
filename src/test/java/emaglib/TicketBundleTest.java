package emaglib;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import java.util.LinkedList;


public class TicketBundleTest  {


    @Test
    public void testBasicSortAllTrainTickets() {
        Ticket t1 = new TrainTicket("A", "B");
        Ticket t2 = new TrainTicket("B", "C");
        Ticket t3 = new TrainTicket("C", "D");

        TicketBundle tb = new TicketBundle();
        tb.add(t2).add(t1).add(t3).sortToDest();

        LinkedList<Ticket> expected = new LinkedList<>();
        expected.add(t1);
        expected.add(t2);
        expected.add(t3);

        assertEquals(tb.getTickets(), expected);
    }

    @Test
    public void testBasicSortAllPlainTickets() {
        Ticket t1 = new PlaneTicket("A", "B");
        Ticket t2 = new PlaneTicket("B", "C");
        Ticket t3 = new PlaneTicket("C", "D");

        TicketBundle tb = new TicketBundle();
        tb.add(t2).add(t1).add(t3).sortToDest();

        LinkedList<Ticket> expected = new LinkedList<>();
        expected.add(t1);
        expected.add(t2);
        expected.add(t3);

        assertEquals(tb.getTickets(), expected);
    }

    @Test
    public void testBasicSortMixedTickets() {
        Ticket t1 = new PlaneTicket("A", "B");
        Ticket t2 = new TrainTicket("B", "C");
        Ticket t3 = new PlaneTicket("C", "D");

        TicketBundle tb = new TicketBundle();
        tb.add(t2).add(t1).add(t3).sortToDest();

        LinkedList<Ticket> expected = new LinkedList<>();
        expected.add(t1);
        expected.add(t2);
        expected.add(t3);

        assertEquals(tb.getTickets(), expected);
    }

    @Test
    public void testNoTicket() {
        TicketBundle tb = new TicketBundle();
        tb.sortToDest();

        LinkedList<Ticket> expected = new LinkedList<>();
        assertEquals(expected, tb.getTickets());
    }

    @Test
    public void testMisplacedTicket() {
        Ticket t1 = new PlaneTicket("A", "B");
        Ticket t2 = new TrainTicket("B", "C");
        Ticket t3 = new PlaneTicket("C", "D");
        Ticket t4 = new PlaneTicket("Z", "F");

        TicketBundle tb = new TicketBundle();
        tb.add(t2).add(t1).add(t3).sortToDest();

        LinkedList<Ticket> expected = new LinkedList<>();
        expected.add(t1);
        expected.add(t2);
        expected.add(t3);
        expected.add(t4);

        expected.remove(t4);

        assertEquals(tb.getTickets(), expected);
    }



}
